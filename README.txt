CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

The ADA Accessible Helper Menu provides a quick solution to allow the user to switch between the active theme and a high contrast version or Text only Version of it. Also, Provide an option to change font size in small, Medium or large.


REQUIREMENTS
------------

This module wokring based on jQuery and CSS only it required JQuery.


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * you must need to clear the cache after installing module.


CONFIGURATION
-------------

 * Configure the Active color and backgrounf color of buttons from the admin panel.
   - Login to admin

   - Go to /admin/config/ada_accessibility_menu

   - Set the color and respective High Contrast or Text view as per requirement.


TROUBLESHOOTING
---------------

 * If the menu does not display, check the following:

   - Are the "ADA Accessible Helper Menu" enabled?

   - Clear the Cache after installing

   - Disable cache to check module is working or not


MAINTAINERS
-----------

Current maintainers:
 * SkynetTechnologies - https://www.drupal.org/user/3479403

This project has been sponsored by:
 * Skynet Technologies
   Web Development company offering vast range of Programming Services, Responsive Web Design, Mobile Application Development (IOS/Android), Shopping Cart Development, CMS Website Development (Drupal, WordPress), SEO Services, Software Website Maintenance, Software Testing Services, Website Security Solutions, IT Consultancy and other Value Added Services.

